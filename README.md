<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Про проект

Quantum - небольшой проект, благодаря которому вы сможете создать рекламный виджет, удобно разместить информацию о пользователе в базе данных, и запросто управлять данными, используя консоль.

    Используемые технологии:
- [Легкий, быстрый Laravel](https://laravel.com).
- [Приятный и понятный Guzzle](http://docs.guzzlephp.org/en/stable/)
- [Browser-detector для максимальной информативности](https://packagist.org/packages/sinergi/browser-detector).

## Установка проекта

- Загрузите проект: **https://gitlab.com/Fully_full/quantum.git**
- **Настройте .env файл и Запустите следующие команды:**
- composer install && npm install && npm run dev
- php artisan key:generate
- php artisan migrate
- php artisan storage:link
- php artisan content:download [--quantity]

### Проблемы:
- нет валидации GET запроса по ссылке: /download?quantity=[количество необходимых изображений]
- после создания публичной ссылки - ее необходимо переименовать в "storage" и создать еще 1 ссылку (должно получиться 2 ссылки: storage и public)
- наляпистость в коде App\Services\Unsplash, необходимо поработать с guzzle

### Решение проблем:
***В процессе***

## License

Quantum is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
