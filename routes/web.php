<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UnsplashController@showWidget');

Route::get('/download', 'UnsplashController@getDataFromUnsplash');

Route::get('/remove', 'UnsplashController@removeAllImages')->name('remove');

Route::get('/redirecting', 'UnsplashController@redirecting')->name('redirecting');
