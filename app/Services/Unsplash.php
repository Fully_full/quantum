<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;



class Unsplash
{
    const SOURCE             = 'https://source.unsplash.com/';
    const SOURCE_URI         = '/random/492x328';
    const PATH_FOR_CONSOLE   = 'app/public/';
    const PATH_FOR_FRONTEND  = 'storage/';

    public function ClientForFrontend() :void
    {
        $client = new Client([
            'base_uri'          => self::SOURCE,
            'allow_redirects'   => ['track_redirects' => true],
            'sink'              => $this->sink(public_path().'/'.self::PATH_FOR_FRONTEND)
        ]);

        $client->get(self::SOURCE_URI);
    }

    public function ClientForConsole() :void
    {
        $client = new Client([
            'base_uri'          => self::SOURCE,
            'allow_redirects'   => ['track_redirects' => true],
            'sink'              => $this->sink(storage_path().'/'.self::PATH_FOR_CONSOLE)
        ]);

        $client->get(self::SOURCE_URI);
    }

    public function sink(string $folderToSaveImage) :string
    {
        $this->createDirIfNotExist($folderToSaveImage);

        return $folderToSaveImage.$this->getFilename();
    }

    public function getFilename() :string
    {
        $client = new Client([
            'base_uri'          => self::SOURCE,
            'allow_redirects'   => ['track_redirects' => true]
        ]);
        $client = $client->get(self::SOURCE_URI);
        $headersRedirect = $client->getHeader(\GuzzleHttp\RedirectMiddleware::HISTORY_HEADER);
        $result = str_replace('/', '', parse_url($headersRedirect[0], PHP_URL_PATH)). '.jpg';

        return $result;
    }

    public function createDirIfNotExist(string $folderToSaveImage) :bool
    {
        if (!is_dir(public_path().'/'.$folderToSaveImage)) {
            Storage::makeDirectory($folderToSaveImage);
        }

        return true;
    }

}
