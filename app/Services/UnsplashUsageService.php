<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class UnsplashUsageService extends Unsplash
{
    public static function getAllImages(): ?array
    {
        return Storage::files('public/');
    }

    public static function getRandomImage(): ?string
    {
        $images = str_replace('public','storage', Storage::files('public/'));

        return ($images) ? $images[array_rand($images, 1)] : null;
    }

    public static function quantityOfImages(): ?int
    {
        return count(self::getAllImages());
    }

    public static function removeAllImages(): void
    {
        Storage::delete(self::getAllImages());
    }

    public static function startDownloading(int $quantity, string $client): void
    {
        $expectedAmount = self::quantityOfImages() + $quantity;

        while (self::quantityOfImages() < $expectedAmount) {
            $unsplash = new Unsplash();
            $unsplash->$client();
            sleep(1);
        }
    }

    public static function downloadingByFrontend(int $quantity)
    {
        self::startDownloading($quantity, 'ClientForFrontend');
    }

    public static function downloadingByConsole(int $quantity)
    {
        self::startDownloading($quantity, 'ClientForConsole');
    }
}
