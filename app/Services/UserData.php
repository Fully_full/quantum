<?php

namespace App\Services;

use App\Models\UserInfo;
use Sinergi\BrowserDetector\{Browser, Os, Device};

class UserData
{
    /**
     * @return string
     */
    protected function getIp()
    {
        return (filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP)) ? $_SERVER['REMOTE_ADDR'] : 'unknown';
    }

    /**
     * @return mixed
     */
    protected function getUserAgent()
    {
        return $_SERVER['HTTP_USER_AGENT'];
    }

    /**
     * @return string
     */
    protected function getDevice() :string
    {
        $device = new Device();

        return $device->getName();
    }

    /**
     * @return string
     */
    protected function getBrowser() :string
    {
        $browser = new Browser();

        return $browser->getName();
    }

    /**
     * @return string
     */
    protected function getOs() :string
    {
        $os = new Os();

        return $os->getName();
    }

    /**
     * @return array
     */
    public function getUserData() :array
    {
        $result = [];
        $result['ip']           = $this->getIp();
        $result['os']           = $this->getOs();
        $result['device']       = $this->getDevice();
        $result['browser']      = $this->getBrowser();
        $result['user_agent']   = $this->getUserAgent();

        return $result;
    }

    public static function recordToDatabase()
    {
        UserInfo::create([
            'info_in_json_format' => \json_encode((new UserData)->getUserData())
        ]);
    }
}
