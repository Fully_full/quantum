<?php

namespace App\Console\Commands;

use App\Services\UnsplashUsageService;
use Illuminate\Console\Command;

class downloadcontent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'content:download {--Q|quantity=20}';

    /**u
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Type to download 20 images or use --quantity=x for x images.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        return UnsplashUsageService::downloadingByConsole($this->options()['quantity']);
    }
}
