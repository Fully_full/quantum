<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    protected $fillable = ['info_in_json_format'];
    protected $table = 'user_info';
}
