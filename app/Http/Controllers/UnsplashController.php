<?php

namespace App\Http\Controllers;

use App\Models\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Cookie, Redirect};
use App\Services\{UnsplashUsageService, UserData};

class UnsplashController extends Controller
{
    public function getDataFromUnsplash()
    {
        $quantity = $_GET['quantity'];
        UnsplashUsageService::downloadingByFrontend($quantity);

        return redirect()->to('/');
    }

    public function showWidget()
    {
        $images = new UnsplashUsageService();

        return view('widget', [
            'widget'        => $images->getRandomImage(),
            'countOfImages' => $images->quantityOfImages()
        ]);
    }

    public function removeAllImages()
    {
        UnsplashUsageService::removeAllImages();

        return redirect()->to('/');
    }

    public function redirecting(Request $request)
    {
        UserData::recordToDatabase();
        $time = Date('Y-M-d > G:i:s');
        Cookie::queue('newUser', 'Date of first visit: '. $time .';', 43200);

        return Redirect::away('http://www.srilankaembassy.ru/ru/');
    }
}
