<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content p-5">
            <form action="/download" method="get">
                <label for="quantity">How many images you want to download? (One request = ~2 sec.)</label><br>
                <input type="number" name="quantity">
                <button type="submit" class="btn btn-dark">Download</button>
            </form>
        </div>
    </div>
</div>
