<link rel="stylesheet" type="text/css" href="https://cdn.wpcc.io/lib/1.0.2/cookieconsent.min.css"/>
<script src="https://cdn.wpcc.io/lib/1.0.2/cookieconsent.min.js"></script>
<script>window.addEventListener("load", function () {
        window.wpcc.init({
            "border": "thin",
            "corners": "small",
            "colors": {
                "popup": {"background": "#cff5ff", "text": "#000000", "border": "#5e99c2"},
                "button": {"background": "#5e99c2", "text": "#ffffff"}
            }
        })
    });</script>
