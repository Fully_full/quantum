@extends('layout')

@section('content')
    @if ($widget)
        <p class="mt-5 d-flex justify-content-center">
            <a href="{{ route('redirecting') }}">
                <img src="{{ $widget }}" alt="Alt">
            </a>
        </p>

        <p class="d-flex justify-content-center">
            <a href="/" class="btn btn-danger ml-1">Reload page</a>
            <a href="#" class="btn btn-danger ml-1" data-toggle="modal" data-target=".bd-example-modal-lg">Add more
                images</a>
            <a href="{{ route('remove') }}" class="btn btn-danger ml-1">Remove all images</a>
        </p>

        <span class="d-flex justify-content-center mt-5">
            <h4>Доступно изображений: {{ $countOfImages }}</h4>
        </span>

    @else
        <div class="mt-4 d-flex justify-content-center">
            <a href="#" class="btn btn-danger mt-4" data-toggle="modal"
               data-target=".bd-example-modal-lg">Add more images</a>
        </div>
    @endif

    <!-- Модальное окно -->
    @include('components/modal_window')

@endsection
